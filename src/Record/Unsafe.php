<?php

$exports["unsafeHas"] = function ($label) {
  return function ($rec) use (&$label) {
    return array_key_exists($label, $rec);
  };
};

$exports["unsafeGet"] = function ($label) {
  return function ($rec) use (&$label) {
    return $rec[$label];
  };
};

$exports["unsafeSet"] = function ($label) {
  return function ($value) use (&$label) {
    return function ($rec) use (&$label, &$value) {
      $copy = $rec; // this shallow copies $rec
      $copy[$label] = $value;
      return $copy;
    };
  };
};

$exports["unsafeDelete"] = function ($label) {
  return function ($rec) use (&$label) {
    $copy = $rec; // this shallow copies $rec
    unset($copy[$label]); // should be safe, since $rec is an associative array
    return $copy;
  };
};
