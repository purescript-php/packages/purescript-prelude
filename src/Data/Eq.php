<?php

$exports["refEq"] = function ($r1) {
  return function ($r2) use (&$r1) {
    return $r1 === $r2;
  };
};

$exports["eqArrayImpl"] = function ($f) {
  return function ($xs) use (&$f) {
    return function ($ys) use (&$xs) {
      if ($xs === $ys) return true;
      if (count($xs) !== count($ys)) return false;
      for ($i = 0; $i < count($xs); $i++) {
        if (!f($xs[i])($ys[i])) return false;
      }
      return true;
    };
  };
};
