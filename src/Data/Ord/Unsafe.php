<?php

$exports["unsafeCompareImpl"] = function ($lt) {
  return function ($eq) use (&$lt) {
    return function ($gt) use (&$lt, &$eq) {
      return function ($x) use (&$lt, &$eq, &$gt) {
        return function ($y) use (&$lt, &$eq, &$gt, &$x) {
          return $x < $y ? $lt : ($x === $y ? $eq : $gt);
        };
      };
    };
  };
};
