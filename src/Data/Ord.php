<?php

$exports["ordArrayImpl"] = function ($f) {
  return function ($xs) use (&$f) {
    return function ($ys) use (&$f, &$xs) {
      $i = 0;
      $xlen = count(xs);
      $ylen = count(ys);
      while ($i < $xlen && $i < $ylen) {
        $x = $xs[$i];
        $y = $ys[$i];
        $o = $f($x)($y);
        if ($o !== 0) {
          return $o;
        }
        $i++;
      }
      if ($xlen === $ylen) {
        return 0;
      } else if ($xlen > $ylen) {
        return -1;
      } else {
        return 1;
      }
    };
  };
};
