<?php

$exports["arrayMap"] = function ($f) {
  return function ($arr) use (&$f) {
    $l = count($arr);
    $result = [];
    for ($i = 0; $i < $l; $i++) {
      $result[$i] = $f($arr[$i]);
    }
    return $result;
  };
};
