<?php

$exports["concatString"] = function ($s1) {
  return function ($s2) use (&$s1) {
    return $s1 . $s2;
  };
};

$exports["concatArray"] = function ($xs) {
  return function ($ys) use (&$xs) {
    if (count($xs) === 0) return $ys;
    if (count($ys) === 0) return $xs;
    return array_merge($xs, $ys);
  };
};
