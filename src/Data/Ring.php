<?php

$exports["intSub"] = function ($x) {
  return function ($y) use ($x) {
    return $x - $y | 0;
  };
};

$exports["numSub"] = function ($n1) {
  return function ($n2) use ($n1) {
    return $n1 - $n2;
  };
};
