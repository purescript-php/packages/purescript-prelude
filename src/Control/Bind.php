<?php

$exports["arrayBind"] = function ($arr) {
  return function ($f) use (&$arr) {
    $result = [];
    $tmp = [];
    for ($i = 0, $l = count($arr); $i < $l; $i++) {
      $tmp = $f($arr[$i]);
      foreach ($tmp as $inner) {
        $result[] = $inner;
      }
    }
    return $result;
  };
};
