<?php

$exports["arrayApply"] = function ($fs) {
  return function ($xs) use (&$fs) {
    $l = count($fs);
    $k = count($xs);
    $result = [];
    $n = 0;
    for ($i = 0; $i < $l; $i++) {
      $f = $fs[$i];
      for ($j = 0; $j < $k; $j++) {
        $result[$n++] = $f($xs[$j]);
      }
    }
    return $result;
  };
};
